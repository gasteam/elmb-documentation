# ELMB documentation

This repository contains various documentation files related to the [Embedded Local Monitoring Board (ELMB)](https://ohwr.org/project/cern-elmb/wikis/home).

The firmware source files and the compiled firmware can be found in [this repo](https://gitlab.cern.ch/atlas-dcs-common-software/ELMBfirmware), in particulare [here](https://gitlab.cern.ch/atlas-dcs-common-software/ELMBfirmware/-/tree/a54c1113b05c104d9ed856b2f50c4844470b1019/GasFlowMeter/firmware).

More documentation can be found in various places:
- [GitLab repository industrial_flow_cells](https://gitlab.cern.ch/fluidic-system/gas/industrial_flow_cells)
- DFS: `G:\Departments\PH\Groups\TA1\Gas_group\ELMB flow meter`
- DFS: `G:\Departments\PH\Groups\TA1\Gas_group\Calibration_Station_Flow_cell`
- DFS: `G:\Departments\PH\Groups\TA1\Gas_group\Industrial Flow Cells Calibration Station`
